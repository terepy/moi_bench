Benchmarking tool to compare the [moi image format](https://crates.io/crates/moi) with qoi, and png  
We used the [qoi crate](https://crates.io/crates/qoi) as it was the fastest encoder/decoder available  
We used [spng](https://libspng.org/) to encode as it compresses equally well as libpng while running much faster  
We used the [image crate](https://crates.io/crates/image) todecode  png images, as it runs faster than spng  
Comparing png at two different compression levels (5 and 9), moi typically beats png in size and performance, with a runtime 2-3x slower than qoi.  
Full results from the [image suite](https://qoiformat.org/benchmark/qoi_benchmark_suite.tar) used in the qoi benchmarks, (except 2 images with broken checksums):  

```
summary for textures_pk
fmt  | encode  | decode  | size
moi  |  1.249s |  0.619s | 32.59%
qoi  |  0.376s |  0.275s | 43.48%
png5 |  5.130s |  0.834s | 51.02%
png9 | 13.589s |  0.739s | 51.32%

summary for textures_pk01
fmt  | encode  | decode  | size
moi  |  0.324s |  0.141s | 30.91%
qoi  |  0.088s |  0.062s | 35.16%
png5 |  1.431s |  0.189s | 31.91%
png9 |  5.947s |  0.183s | 31.86%

summary for icon_512
fmt  | encode  | decode  | size
moi  |  0.378s |  0.242s |  6.75%
qoi  |  0.139s |  0.088s |  8.35%
png5 |  2.778s |  0.275s |  5.06%
png9 | 14.405s |  0.261s |  4.56%

summary for photo_wikipedia
fmt  | encode  | decode  | size
moi  |  1.460s |  0.688s | 43.84%
qoi  |  0.444s |  0.424s | 49.64%
png5 |  6.752s |  0.977s | 48.90%
png9 | 19.067s |  0.972s | 47.89%

summary for pngimg
fmt  | encode  | decode  | size
moi  |  3.927s |  2.229s | 16.65%
qoi  |  1.329s |  1.153s | 20.33%
png5 | 23.763s |  3.405s | 17.55%
png9 | 96.516s |  3.358s | 16.75%

summary for textures_pk02
fmt  | encode  | decode  | size
moi  |  1.870s |  0.938s | 35.59%
qoi  |  0.497s |  0.375s | 40.38%
png5 |  8.040s |  0.979s | 35.61%
png9 | 36.116s |  0.996s | 35.32%

summary for textures_plants
fmt  | encode  | decode  | size
moi  |  0.759s |  0.396s | 18.88%
qoi  |  0.251s |  0.220s | 22.19%
png5 |  4.633s |  0.645s | 21.14%
png9 | 16.645s |  0.645s | 20.42%

summary for icon_64
fmt  | encode  | decode  | size
moi  |  0.015s |  0.008s | 24.81%
qoi  |  0.005s |  0.003s | 28.70%
png5 |  0.076s |  0.013s | 23.00%
png9 |  0.412s |  0.012s | 22.97%

summary for photo_kodak
fmt  | encode  | decode  | size
moi  |  0.251s |  0.116s | 36.65%
qoi  |  0.073s |  0.051s | 43.71%
png5 |  1.167s |  0.160s | 46.05%
png9 |  3.619s |  0.168s | 46.34%

summary for textures_photo
fmt  | encode  | decode  | size
moi  |  0.583s |  0.273s | 41.58%
qoi  |  0.162s |  0.149s | 48.39%
png5 |  2.491s |  0.363s | 44.71%
png9 |  7.368s |  0.377s | 47.89%

summary for screenshot_web
fmt  | encode  | decode  | size
moi  |  0.703s |  0.561s |  6.40%
qoi  |  0.270s |  0.216s |  8.35%
png5 |  5.615s |  0.780s |  7.31%
png9 | 23.803s |  0.764s |  7.42%

summary for photo_tecnick
fmt  | encode  | decode  | size
moi  |  3.639s |  1.839s | 40.13%
qoi  |  1.175s |  1.116s | 44.94%
png5 | 18.310s |  2.530s | 44.13%
png9 | 66.023s |  2.583s | 42.37%

summary for screenshot_game
fmt  | encode  | decode  | size
moi  |  5.587s |  3.140s | 17.59%
qoi  |  1.967s |  1.579s | 20.99%
png5 | 31.088s |  3.976s | 18.53%
png9 | 209.39s |  3.962s | 17.50%

summary for all folders
fmt  | encode  | decode  | size
moi  | 20.745s | 11.192s | 21.65%
qoi  |  6.774s |  5.712s | 25.58%
png5 | 111.28s | 15.128s | 23.66%
png9 | 512.90s | 15.021s | 22.92%
```
