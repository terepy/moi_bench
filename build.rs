use std::env;

fn main() {
    let libz_include = env::var_os("DEP_Z_INCLUDE").unwrap(); //this line is failing

    let sse = if cfg!(target_feature = "sse4.1") { "4" } else { "3" };
    
    cc::Build::new()
        .file("spng/spng.c")
        .define("SPNG_SSE", Some(sse))
        .include(libz_include)
        .include("spng")
        .compile("libspng.a");

    println!("cargo:include=libspng/spng");
    println!("cargo:rustc-link-lib=static={}", libname());
}

fn libname() -> &'static str {
    let target = env::var("TARGET").unwrap();
    // Derived from: https://github.com/rust-lang/libz-sys/blob/36b3071331d9a87712c9d23fd7aea79208425c73/build.rs#L167
    if target.contains("windows") {
        if target.contains("msvc") && env::var("OPT_LEVEL").unwrap() == "0" {
            "zlibd"
        } else {
            "zlib"
        }
    } else {
        "z"
    }
}
