use moi::Image;

use std::os::raw::{c_void, c_int};

//TODO: is there a better way to do this? link to the library instead of hard coded constants?
const SPNG_CTX_ENCODER: i32 = 2;
const SPNG_COLOR_TYPE_TRUECOLOR_ALPHA: u8 = 6;
const SPNG_ENCODE_TO_BUFFER: i32 = 12;
const SPNG_FMT_PNG: i32 = 256;
const SPNG_ENCODE_FINALIZE: i32 = 2;
const SPNG_IMG_COMPRESSION_LEVEL: i32 = 2;
const SPNG_FMT_RGBA8: i32 = 1;
const SPNG_DECODE_TRNS: i32 = 1;

#[repr(C)]
#[derive(Default)]
struct SpngIhdr {
    width: u32,
    height: u32,
    bit_depth: u8,
    color_type: u8,
    compression_method: u8,
    filter_method: u8,
    interlace_method: u8,
}

#[link(name = "spng")]
extern "C" {
    fn spng_ctx_new(flags: c_int) -> *mut c_void;
    fn spng_set_option(ctx: *mut c_void, option: c_int, value: c_int);
    fn spng_set_ihdr(ctx: *mut c_void, ihdr: *mut SpngIhdr);
    fn spng_encode_image(ctx: *mut c_void, input: *mut c_void, input_len: usize, output_format: c_int, flags: c_int) -> c_int;
    fn spng_get_png_buffer(ctx: *mut c_void, out_len: *mut usize, error: *mut c_int) -> *mut c_void;
    fn spng_ctx_free(ctx: *mut c_void);
    fn spng_set_png_buffer(ctx: *mut c_void, buf: *mut c_void, bufsize: usize) -> c_int;
    fn spng_get_ihdr(ctx: *mut c_void, ihdr: *mut SpngIhdr) -> c_int;
    fn spng_decoded_image_size(ctx: *mut c_void, fmt: c_int, size: *mut usize) -> c_int;
    fn spng_decode_image(ctx: *mut c_void, image: *mut c_void, size: usize, fmt: c_int, flags: c_int) -> c_int;
}

pub fn encode_png_image(image: &Image, compression_rate: i32) -> Vec<u8> {
    unsafe {
        let ctx = spng_ctx_new(SPNG_CTX_ENCODER);

        let mut ihdr = SpngIhdr {
            width: image.width,
            height: image.height,
            color_type: SPNG_COLOR_TYPE_TRUECOLOR_ALPHA,
            bit_depth: 8,
            .. Default::default()
        };

        spng_set_option(ctx, SPNG_ENCODE_TO_BUFFER, 1);
        spng_set_option(ctx, SPNG_IMG_COMPRESSION_LEVEL, compression_rate);
        
        spng_set_ihdr(ctx, &mut ihdr);
        
        let mut ret = spng_encode_image(ctx, image.pixels.as_ptr() as *mut _, image.pixels.len() * 4, SPNG_FMT_PNG, SPNG_ENCODE_FINALIZE);

        if ret != 0 {
            panic!("spng error");
        }

        let mut png_size = 0;
        let png_buf = spng_get_png_buffer(ctx, &mut png_size, &mut ret);

        if png_buf.is_null() {
            panic!("spng error");
        }

        let png_bytes: Vec<u8> = Vec::from_raw_parts(png_buf as *mut _, png_size as usize, png_size as usize);

        spng_ctx_free(ctx);

        png_bytes
    }
}

pub fn decode_png_image(buf: &[u8]) -> Image {
    unsafe {
        let ctx = spng_ctx_new(0);
        assert!(!ctx.is_null(), "spng error");

        let ret = spng_set_png_buffer(ctx, buf.as_ptr() as *mut _, buf.len());
        assert!(ret == 0, "spng error");

        let mut ihdr = SpngIhdr::default();
        let ret = spng_get_ihdr(ctx, &mut ihdr);
        assert!(ret == 0, "spng error");

        let mut size = 0;
        let ret = spng_decoded_image_size(ctx, SPNG_FMT_RGBA8, &mut size);
        assert!(ret == 0, "spng error");

        assert!(size % 4 == 0);
        let mut pixels = vec![[0u8; 4]; size as usize / 4];
        let ret = spng_decode_image(ctx, pixels.as_mut_ptr() as *mut _, size, SPNG_FMT_RGBA8, SPNG_DECODE_TRNS);
        assert!(ret == 0, "spng error");

        spng_ctx_free(ctx);

        Image {
            pixels,
            width: ihdr.width,
            height: ihdr.height,
        }
    }
}
