use std::time::Duration;
use std::fmt;
use std::ops::{Index, IndexMut};

use crate::{PNGF_LEVEL,ENABLED};

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum ImgFmt {
    Moi = 0,
    Qoi = 1,
    Pngf = 2,
    Png9 = 3,
}
pub use ImgFmt::*;

impl fmt::Display for ImgFmt {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            Moi => write!(f, "moi"),
            Qoi => write!(f, "qoi"),
            Pngf => write!(f, "png{}", PNGF_LEVEL),
            Png9 => write!(f, "png9"),
        }
    }
}

impl ImgFmt {
    pub fn enabled(self) -> bool {
        ENABLED[self as usize]
    }
}

#[derive(Default, Clone)]
pub struct Stats {
    pub raw_size: usize,
    pub stats: [Option<EncodingStats>; 4],
}

#[derive(Clone)]
pub struct EncodingStats {
    pub encode_time: Duration,
    pub decode_time: Duration,
    pub size: usize,
    pub frequencies: [(u8, usize); 256],
}

impl Default for EncodingStats {
    fn default() -> Self {
        let mut frequencies = [(0, 0); 256];
        for i in 0..256 {
            frequencies[i].0 = i as u8;
        }
        
        Self {
            encode_time: Duration::default(),
            decode_time: Duration::default(),
            size: 0,
            frequencies,
        }
    }
}

impl Index<ImgFmt> for Stats {
    type Output = Option<EncodingStats>;

    fn index(&self, fmt: ImgFmt) -> &Self::Output {
        &self.stats[fmt as usize]
    }
}

impl IndexMut<ImgFmt> for Stats {
    fn index_mut(&mut self, fmt: ImgFmt) -> &mut Self::Output {
        &mut self.stats[fmt as usize]
    }
}

impl Stats {
    pub fn summarize(&self) {
        println!("fmt  | encode  | decode | size");
        for &fmt in &[Moi, Qoi, Pngf, Png9] {
            self[fmt].as_ref().map(|x| x.summarize(fmt, self.raw_size));
        }
    }
}

impl EncodingStats {
    pub fn summarize(&self, fmt: ImgFmt, raw_size: usize) {
        let name = fmt.to_string();
        let encode_time_secs = self.encode_time.as_secs_f32();
        let decode_time_secs = self.decode_time.as_secs_f32();
        let size_percentage = (self.size as f32 / raw_size as f32) * 100.0;

        println!("{:<4} | {:>6.2}s | {:>5.2}s | {:>5.2}%",name,encode_time_secs,decode_time_secs,size_percentage);
    }
    
    pub fn summarize_freqs(&self, max_lines: usize) {
        if self.size == 0 {
            return;
        }

        for (byte, count) in self.frequencies.iter().take(max_lines) {
            let percentage = (*count as f64 / self.size as f64) * 100.0;
            println!("{:3}: {:.2}%", byte, percentage);
        }

        let encoding = crate::huffman::build_huffman_tree(&self.frequencies);
        let mut total = 0;
        for &(byte, count) in &self.frequencies {
            total += encoding[byte as usize].0 * count;
        }
        println!("expected size after huffman tree: {:.2}%",total as f64 / 8.0 / self.size as f64 * 100.0);

        let probabilities: Vec<f64> = self.frequencies.iter().map(|&freq| freq.1 as f64 / self.size as f64).collect();

        let entropy = probabilities
            .iter()
            .filter(|&&p| p != 0.0)
            .map(|&p| -p * p.log2())
            .sum::<f64>();

        println!("theoretical entropy: {:.2}%",entropy / 8.0 * 100.0);
    }
}

impl std::ops::AddAssign for EncodingStats {
    fn add_assign(&mut self, other: EncodingStats) {
        self.encode_time += other.encode_time;
        self.decode_time += other.decode_time;
        self.size += other.size;
    }
}
