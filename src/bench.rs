use std::collections::HashMap;
use crate::folder::*;
use crate::stats::*;
use moi::Image;
use std::ops::{Deref,DerefMut};

#[derive(Default)]
pub struct BenchMgr {
    pub folders: HashMap<String, Folder>,
    pub stats: Stats,
}

impl Deref for BenchMgr {
    type Target = Stats;

    fn deref(&self) -> &Self::Target {
        &self.stats
    }
}

impl DerefMut for BenchMgr {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.stats
    }
}

impl BenchMgr {
    pub fn add_raw(&mut self, dir: &str, images: Vec<(Image, String)>, raw_size: usize) {
        self.raw_size += raw_size;
        let mut f = Folder::default();
        f.images = images.into_iter().map(|(raw, path)| ImageData { path, raw, .. Default::default() }).collect();
        f.raw_size = raw_size;
        self.folders.insert(dir.to_string(), f).map(|_| println!("error inserted a folder that already existed"));
    }

    pub fn measure(&mut self, fmt: ImgFmt) {
        self.stats[fmt] = Some(EncodingStats::default());
        let stats = self.stats[fmt].as_mut().unwrap();
        
        for folder in self.folders.values_mut() {
            folder.measure(fmt);
            *stats += folder.stats[fmt].clone().unwrap();
        }
    }

    pub fn count_freqs(&mut self) {
        for fmt in [Moi, Qoi, Pngf, Png9] {
            if !fmt.enabled() { continue; }
            let stats = self.stats[fmt].as_mut().unwrap();
            for (_folder_name, folder) in self.folders.iter_mut() {
                folder.count_freqs(fmt);

                let folder_stats = &folder.stats[fmt].as_ref().unwrap();

                for &(i, count) in &folder_stats.frequencies {
                    stats.frequencies[i as usize].1 += count;
                }
            }
            stats.frequencies.sort_unstable_by_key(|x| !x.1);
        }
    }
    
    pub fn summarize(&self) {
        for (folder_name, folder) in &self.folders {
            println!("summary for {}", folder_name);
            folder.stats.summarize();
            println!();
        }
        println!("summary for all folders");
        self.stats.summarize();
    }

    pub fn summarize_freqs(&self, fmt: ImgFmt, max_lines: usize) {
        for (folder_name, folder) in &self.folders {
            if let Some(stats) = folder.stats[fmt].as_ref() {
                println!("Frequency summary for {}:", folder_name);
                stats.summarize_freqs(max_lines);
                println!();
            }
        }

        if let Some(stats) = self.stats[fmt].as_ref() {
            println!("Frequency summary for all folders");
            stats.summarize_freqs(max_lines);
        }
    }
}
