use moi::Image;
use crate::stats::*;
use std::mem::ManuallyDrop;
use std::ops::{Deref, DerefMut};
use crate::png_ffi::{encode_png_image, decode_png_image};
use crate::{USE_IMAGE_CRATE_DECODER, PNGF_LEVEL, STORE_NON_MOI};
use std::ops::{Index,IndexMut};

use std::time::Instant;

#[derive(Default, Clone)]
pub struct ImageData {
    pub path: String,
    pub raw: Image,
    pub data: [Vec<u8>; 4],
}

impl Index<ImgFmt> for ImageData {
    type Output = Vec<u8>;

    fn index(&self, index: ImgFmt) -> &Self::Output {
        &self.data[index as usize]
    }
}

impl IndexMut<ImgFmt> for ImageData {
    fn index_mut(&mut self, index: ImgFmt) -> &mut Self::Output {
        &mut self.data[index as usize]
    }
}

#[derive(Default, Clone)]
pub struct Folder {
    pub images: Vec<ImageData>,
    pub stats: Stats,
}

impl Deref for Folder {
    type Target = Stats;

    fn deref(&self) -> &Self::Target {
        &self.stats
    }
}

impl DerefMut for Folder {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.stats
    }
}

fn image_as_bytes(image: &Image) -> &[u8] {
    let ptr = image.pixels.as_slice().as_ptr();
    let len = image.pixels.len() * 4;
    unsafe { std::slice::from_raw_parts(ptr as *const u8, len) }
}

fn image_from_bytes(bytes: Vec<u8>, width: u32, height: u32) -> Image {
    let (ptr, len, cap) = (bytes.as_ptr(), bytes.len(), bytes.capacity());
    assert!(len % 4 == 0);
    let _ = ManuallyDrop::new(bytes);
    let pixels = unsafe { Vec::from_raw_parts(ptr as *mut [u8; 4], len / 4, cap / 4) };
    
    Image {
        pixels,
        width,
        height,
    }
}

fn decode_png(bytes: &[u8]) -> Image {
    if USE_IMAGE_CRATE_DECODER {
        let decoded = image::load_from_memory_with_format(bytes, image::ImageFormat::Png)
            .expect("png decoding failed").to_rgba8();
        let (width, height) = decoded.dimensions();
        image_from_bytes(decoded.into_vec(), width, height)
    } else {
        decode_png_image(bytes)
    }
}

fn encode(image: &Image, fmt: ImgFmt) -> Vec<u8> {
    match fmt {
        Moi => image.encode(),
        Qoi => {
            let bytes = image_as_bytes(image);
            qoi::encode_to_vec(bytes, image.width, image.height).expect("qoi encoding failed")
        },
        Pngf => encode_png_image(image, PNGF_LEVEL),
        Png9 => encode_png_image(image, 9),
    }
}

fn decode(data: &[u8], fmt: ImgFmt) -> Result<Image, &'static str> {
    match fmt {
        Moi => Image::decode(data),
        Qoi => {
            let (header, decoded) = qoi::decode_to_vec(data).expect("qoi decoding failed");
            let pixels = image_from_bytes(decoded, header.width, header.height);
            Ok(pixels)
        },
        Pngf | Png9 => Ok(decode_png(data)),
    }
}

impl Folder {
    fn encode(&mut self, fmt: ImgFmt) {
        self.stats[fmt] = Some(Default::default());
        let stats = self.stats[fmt].as_mut().unwrap();

        for image in &mut self.images {
            let start = Instant::now();
            let encoded = encode(&image.raw, fmt);
            stats.encode_time += start.elapsed();

            stats.size += encoded.len();
            image[fmt] = encoded;
        }
    }

    fn decode(&mut self, fmt: ImgFmt) {
        if self.stats[fmt].is_none() { return; } //can't decode if we got not encoding data
        let stats = self.stats[fmt].as_mut().unwrap();

        for image in &self.images {
            let start = Instant::now();
            let decoded = decode(&image[fmt], fmt);
            stats.decode_time += start.elapsed();

            match decoded {
                Ok(decoded) => if decoded != image.raw {
                    println!("error decoding {}: mismatch", image.path);
                },
                Err(e) => println!("error decoding {}: {}", image.path, e),
            }
        }
    }
    
    pub fn measure(&mut self, fmt: ImgFmt) {
        self.encode(fmt);
        self.decode(fmt);
        if !STORE_NON_MOI && fmt != Moi {
            self.images.iter_mut().for_each(|x| x[fmt].clear());
        }
    }
    
    pub fn count_freqs(&mut self, fmt: ImgFmt) {
        let stats = self.stats[fmt].as_mut().unwrap();
        for image in &mut self.images {
            for &byte in &image[fmt] {
                stats.frequencies[byte as usize].1 += 1;
            }
        }
        stats.frequencies.sort_unstable_by_key(|x| !x.1);
    }
}
