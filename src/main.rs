use moi::Image;
use std::fs::{read,read_dir};
use std::error::Error;
use std::path::PathBuf;

mod stats;
mod bench;
mod folder;
mod png_ffi;
mod huffman;
use bench::BenchMgr;
use stats::*;

const IMAGE_DIR: &str = "qoi_images";
const PNGF_LEVEL: i32 = 5;
const ENABLED: [bool; 4] = [
    true, //moi
    true, //qoi
    false, //pngf
    false, //png9
];
const COUNT_FREQS: bool = false;
const PRINT_FREQS: [bool; 4] = [
    true, //moi
    true, //qoi
    true, //pngf
    true, //png9
];
const FREQ_MAX_LINES: usize = 10;

const STORE_NON_MOI: bool = true; //we don't really need to store other image formats because we're not doing analysis on them
const USE_IMAGE_CRATE_DECODER: bool = true;

fn main() {
    let mut mgr = BenchMgr::default();
    for entry in read_dir(IMAGE_DIR).expect("failed to read images directory") {
        let dir = entry.unwrap();
        let path = dir.path();
        let (images, raw_size) = load_images(&path).expect("reading images");
        let name = path.file_name().expect("no file name").to_str().expect("no file name");
        mgr.add_raw(name, images, raw_size);
    }
    println!("loaded {:.2} raw GB",mgr.raw_size as f32 / 2f32.powi(30));

    for fmt in [Moi,Qoi,Pngf,Png9] {
        if fmt.enabled() {
            println!("measuring {}",fmt);
            mgr.measure(fmt);
        }
    }
    println!();

    if COUNT_FREQS {
        println!("counting frequencies");
        mgr.count_freqs();
    }
    
    if COUNT_FREQS {
        println!();
        for fmt in [Moi,Qoi,Pngf,Png9] {
            if fmt.enabled() && PRINT_FREQS[fmt as usize] {
                println!("frequencies for {}",fmt);
                mgr.summarize_freqs(fmt, FREQ_MAX_LINES);
                println!()
            }
        }
    }
    
    mgr.summarize();
}

fn load_images(dir: &PathBuf) -> Result<(Vec<(Image, String)>, usize), Box<dyn Error>> {
    let mut images = Vec::new();
    let mut raw_size = 0;

    for entry in read_dir(dir)? {
        let path = entry?.path();
        let name = path.file_name().ok_or("no file name")?.to_str().ok_or("no file name")?.to_string();
        let data = read(&path)?;
        let extension = path.extension().ok_or("no file name")?.to_str().ok_or("no file name")?;
        let image = match extension {
            "moi" => Image::decode(&data)?,
            "qoi" => {
                let (header, bytes) = qoi::decode_to_vec(&data).expect("opening qoi image");
                let mut image = Image {
                    width: header.width,
                    height: header.height,
                    pixels: bytes.chunks_exact(4).map(|x| x.try_into().unwrap()).collect(),
                };
                image.pixels.truncate((image.width * image.height) as usize);
                image
            },
            _ => {
                match image::load_from_memory(&data) {
                    Ok(image) => {
                        let image = image.to_rgba8();
                        let (width, height) = image.dimensions();
                        let mut image = Image {
                            width,
                            height,
                            pixels: image.into_raw().chunks_exact(4).map(|x| x.try_into().unwrap()).collect(),
                        };
                        image.pixels.truncate(width as usize * height as usize);
                        image
                    },
                    Err(e) => {
                        println!("error reading {}: {}",name,e);
                        continue;
                    }
                }
            },
        };

        raw_size += image.pixels.len() * 4;
        images.push((image, name));
    }
    
    Ok((images, raw_size))
}
