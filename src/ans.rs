pub fn build_ans_encodings(frequencies: &[(u8); 256]) -> HashMap<usize, (usize, usize)> {
    let mut sorted_frequencies = self.frequencies
        .iter()
        .enumerate()
        .filter(|&(_, &(_, freq))| freq > 0)
        .map(|(byte, &(_, freq))| (byte, freq))
        .collect::<Vec<_>>();

    sorted_frequencies.sort_unstable_by_key(|&(_, freq)| freq);

    let sum_frequencies = sorted_frequencies
        .iter()
        .map(|&(_, freq)| freq)
        .sum::<usize>();

    let mut normalization_factor = 1;
    while (sum_frequencies * normalization_factor) & (sum_frequencies * normalization_factor - 1) != 0 {
        normalization_factor += 1;
    }

    let mut state = sum_frequencies * normalization_factor;
    let mut encoding_table = HashMap::new();

    for &(byte, freq) in sorted_frequencies.iter().rev() {
        for _ in 0..freq {
            encoding_table.insert(state / normalization_factor, (byte, state % normalization_factor));
            state -= 1;
        }
    }

    encoding_table
}
