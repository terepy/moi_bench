use std::collections::BinaryHeap;
use std::cmp::Ordering;

#[derive(Default,Eq)]
struct HuffmanNode {
    byte: Option<u8>,
    freq: usize,
    left: Option<Box<HuffmanNode>>,
    right: Option<Box<HuffmanNode>>,
}

// Implement `PartialEq` and `Ord` manually to create a min-heap instead of a max-heap
impl PartialEq for HuffmanNode {
    fn eq(&self, other: &Self) -> bool {
        self.freq.eq(&other.freq)
    }
}

impl PartialOrd for HuffmanNode {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.freq.cmp(&other.freq).reverse())
    }
}

impl Ord for HuffmanNode {
    fn cmp(&self, other: &Self) -> Ordering {
        self.freq.cmp(&other.freq).reverse()
    }
}

pub fn build_huffman_tree(frequencies: &[(u8, usize); 256]) -> [(usize, usize); 256] {
    let mut heap = BinaryHeap::new();
    for &(byte, count) in frequencies.iter().rev() {
        let node = HuffmanNode {
            byte: Some(byte),
            freq: count,
            left: None,
            right: None,
        };
        heap.push(node);
    }

    while heap.len() > 1 {
        let node1 = heap.pop().unwrap();
        let node2 = heap.pop().unwrap();

        let combined_node = HuffmanNode {
            byte: None,
            freq: node1.freq + node2.freq,
            left: Some(Box::new(node1)),
            right: Some(Box::new(node2)),
        };

        heap.push(combined_node);
    }

    let mut r = [(0, 0); 256];
    generate_encoding(&heap.pop().unwrap(), 0, 0, &mut r);
    r
}

fn generate_encoding(node: &HuffmanNode, length: usize, value: usize, encoding: &mut [(usize, usize); 256]) {
    match node.byte {
        Some(byte) => {
            encoding[byte as usize] = (length, value);
        }
        None => {
            if let Some(left) = &node.left {
                generate_encoding(left, length + 1, value << 1, encoding);
            }
            if let Some(right) = &node.right {
                generate_encoding(right, length + 1, (value << 1) | 1, encoding);
            }
        }
    }
}
